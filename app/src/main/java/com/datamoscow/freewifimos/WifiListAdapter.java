package com.datamoscow.freewifimos;

/*
* Custom adapter for listview with Wi-Fi Points
*/

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class WifiListAdapter extends ArrayAdapter<WiFiPoint>{

    private ArrayList<WiFiPoint> dataSet;
    Context mContext;

    private static class ViewHolder {
        TextView txtTitleObject;
        TextView txtAddressObject;
        //ImageView imgWifi;
    }

    public WifiListAdapter(ArrayList<WiFiPoint> data, Context context) {
        super(context, R.layout.wifi_list_row, data);
        this.dataSet = data;
        this.mContext = context;
    }

    private int lastPosition = -1;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        WiFiPoint dataModel = getItem(position);

        ViewHolder viewHolder;

        final View result;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.wifi_list_row, parent, false);
            viewHolder.txtTitleObject = convertView.findViewById(R.id.row_title_object);
            viewHolder.txtAddressObject = convertView.findViewById(R.id.row_address_object);

            result = convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result = convertView;
        }

        lastPosition = position;

        viewHolder.txtTitleObject.setText(dataModel.getCells().getFullName());
        viewHolder.txtAddressObject.setText(dataModel.getCells().getFullAddress());
        return convertView;
    }

}
