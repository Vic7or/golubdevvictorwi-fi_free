package com.datamoscow.freewifimos;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    ArrayList<WiFiPoint> pointList; // <- Тут у нас весь список!
    WifiListAdapter wifiListAdapter;
    ListView listView;

    DataLoad dataLoad;
    int selectedCategory = DataCategory.CITY_WIFI;

    ConstraintLayout loadWindow;
    ProgressBar pBar;
    TextView txtError;
    Button btnRetry;
    FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.getMenu().getItem(0).setChecked(true);
        navigationView.setItemIconTintList(null);

        loadWindow = findViewById(R.id.loadWindow);
        pBar       = findViewById(R.id.pBar);
        txtError   = findViewById(R.id.txtError);
        btnRetry   = findViewById(R.id.btnRetry);
        fab        = findViewById(R.id.fab);

        listView = findViewById(R.id.lvWifi);

        btnRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dataLoad = new DataLoad();
                dataLoad.execute(selectedCategory);
            }
        });

        // Загрузка данных нужной категории
        dataLoad = new DataLoad();
        dataLoad.execute(selectedCategory);




        fab.setOnClickListener(new View.OnClickListener() {
            //Нажатие на кнопку фильтрации
            @Override
            public void onClick(View view) {
                Intent filterIntent = new Intent(getApplicationContext(), FilterActivity.class);
                String[] Areas=new String[pointList.size()+1];
                String[] AdmAreas=new String[pointList.size()+1];
                int index1=0;
                int index2=0;
                for (int i=0; i<pointList.size(); i++)
                {
                    int err1=0;
                    for(int j=0; j<index1; j++) {
                        String mas_str=Areas[j];
                        String mas_list=pointList.get(i).getCells().getDistrict();
                        if (mas_str.equals(mas_list))
                            err1 = 1;
                    }
                    if(err1==0)
                    {
                        Areas[index1]=pointList.get(i).getCells().getDistrict();
                        index1++;
                    }

                    int err2=0;
                    for(int j=0; j<index2; j++) {
                        String mas_str=AdmAreas[j];
                        String mas_list=pointList.get(i).getCells().getAdmArea();
                        if (mas_str.equals(mas_list))
                            err2 = 1;
                    }
                    if(err2==0)
                    {
                        AdmAreas[index2]=pointList.get(i).getCells().getAdmArea();
                        index2++;
                    }
                }
                Areas=SortMassAreas(Areas,index1);
                AdmAreas=SortMassAdmAreas(AdmAreas,index2);

                filterIntent.putExtra("points1", Areas);
                filterIntent.putExtra("points2", AdmAreas);
                startActivityForResult(filterIntent,1);
            }
        });
    }

    //Получили результат из фильтра
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null) {return;}
        String resArea = data.getStringExtra("Area");
        String resAdmArea = data.getStringExtra("AdmArea");
        String resWithout = data.getStringExtra("Without");

        ArrayList<WiFiPoint> pointListFilter=null;
        if(resArea.equals("Все районы...") || resAdmArea.equals("Все округа...") || resWithout.equals("+"))
        {
            wifiListAdapter = new WifiListAdapter(pointList, getApplicationContext());
            listView.setAdapter(wifiListAdapter);
            Toast.makeText(getApplicationContext(),"Полный список",Toast.LENGTH_SHORT).show();
        }
        else if(!resArea.equals("0") && resAdmArea.equals("0") && resWithout.equals("0"))
        {
            //Фильтр по району
            int sizeA=0;
            for(int i=0; i<pointList.size(); i++)
            {
                if(resArea.equals(pointList.get(i).getCells().getDistrict()))
                    sizeA++;
            }
            pointListFilter=new ArrayList<>(sizeA);
            for(int i=0; i<pointList.size(); i++)
            {
                if(resArea.equals(pointList.get(i).getCells().getDistrict()))
                    pointListFilter.add(pointList.get(i));
            }
            wifiListAdapter = new WifiListAdapter(pointListFilter, getApplicationContext());
            listView.setAdapter(wifiListAdapter);
            Toast.makeText(getApplicationContext(),"Выбор: "+resArea,Toast.LENGTH_SHORT).show();
        }
        else if(resArea.equals("0") && !resAdmArea.equals("0") && resWithout.equals("0"))
        {
            //Фильтр по административным округам
            int sizeA=0;
            for(int i=0; i<pointList.size(); i++)
            {
                String AdmAreaPL=pointList.get(i).getCells().getAdmArea();
                if(AdmAreaPL.contains(resAdmArea))
                    sizeA++;
            }
            pointListFilter=new ArrayList<>(sizeA);
            for(int i=0; i<pointList.size(); i++)
            {
                String AdmAreaPL=pointList.get(i).getCells().getAdmArea();
                if(AdmAreaPL.contains(resAdmArea))
                    pointListFilter.add(pointList.get(i));
            }
            wifiListAdapter = new WifiListAdapter(pointListFilter, getApplicationContext());
            listView.setAdapter(wifiListAdapter);
            Toast.makeText(getApplicationContext(),"Выбор: "+resAdmArea,Toast.LENGTH_SHORT).show();
        }


    }

    //Сортировка массива наименований районов перед отправкой в фильтр
    private String[] SortMassAreas(String[] mas, int lngth)
    {
        String[] Otvet=new String[lngth+1];
        for(int i=0;i<lngth;i++)
        {
            if(mas[i].indexOf("район") == 0) {
                Otvet[i]=mas[i].charAt(6)+mas[i];
            }
            else
            {
                Otvet[i]=mas[i].charAt(0)+mas[i];
            }
        }
        Otvet[lngth]="яяя";
        Arrays.sort(Otvet);
        for(int i=lngth;i>0;i--)
        {
            Otvet[i-1]=Otvet[i-1].substring(1);
            Otvet[i]= Otvet[i - 1];
        }
        Otvet[0]="Все районы...";
        return Otvet;
    }
    //Сортировка массива наименований административных округов перед отправкой в фильтр
    private String[] SortMassAdmAreas(String[] mas, int lngth)
    {
        String[] Otvet=new String[lngth+1];
        Otvet[0]="Все округа...";
        for(int i=1;i<lngth+1;i++)
        {
            Otvet[i]=mas[i-1].substring(0,mas[i-1].indexOf(' '));
        }
        return Otvet;
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    public void onRefresh(View view){
        globals.refresh = true;
        dataLoad = new DataLoad();
        dataLoad.execute(selectedCategory);
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        Boolean exit_selected = false;

        if (id == R.id.nav_city) {
            selectedCategory = DataCategory.CITY_WIFI;
        } else if (id == R.id.nav_library) {
            selectedCategory = DataCategory.LIBRARY_WIFI;
        } else if (id == R.id.nav_park) {
            selectedCategory = DataCategory.PARKS_WIFI;
        } else if (id == R.id.nav_culture) {
            selectedCategory = DataCategory.CULTURAL_CENTER_WIFI;
        } else if (id == R.id.nav_cinema) {
            selectedCategory = DataCategory.CINEMA_WIFI;
        } else if (id == R.id.nav_exit) {
            exit_selected = true;
        }

        if(!exit_selected) {
            dataLoad = new DataLoad();
            dataLoad.execute(selectedCategory);

            DrawerLayout drawer = findViewById(R.id.drawer_layout);
            drawer.closeDrawer(GravityCompat.START);
            return true;
        }
        else{
            android.os.Process.killProcess(android.os.Process.myPid());
            return true;
        }

    }


    @SuppressLint("StaticFieldLeak")
    class DataLoad extends AsyncTask<Integer, Void, List<WiFiPoint>> {

        int Category;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loadWindow.setVisibility(View.VISIBLE);
            pBar.setVisibility(View.VISIBLE);
            fab.setVisibility(View.GONE);
            listView.setVisibility(View.GONE);
            txtError.setVisibility(View.GONE);
            btnRetry.setVisibility(View.GONE);
        }

        @Override
        protected List<WiFiPoint> doInBackground(Integer... params) {
            try
            {
                Category = params[0];


                // Чтение данных из файла //////////////////////////////////////////////////////
                String filename = "";

                switch (Category){
                    case DataCategory.CINEMA_WIFI: filename = "wifi-cinema.txt"; break;
                    case DataCategory.CITY_WIFI: filename = "wifi-city.txt"; break;
                    case DataCategory.CULTURAL_CENTER_WIFI: filename = "wifi-cult.txt"; break;
                    case DataCategory.LIBRARY_WIFI: filename = "wifi-lib.txt"; break;
                    case DataCategory.PARKS_WIFI: filename = "wifi-park.txt"; break;
                }

                if(!globals.refresh) {
                    boolean readed;

                    String readedJson = "";

                    try {
                        FileInputStream fis = openFileInput(filename);
                        InputStreamReader isr = new InputStreamReader(fis);
                        BufferedReader bufferedReader = new BufferedReader(isr);
                        String line;
                        StringBuilder sb = new StringBuilder();
                        while ((line = bufferedReader.readLine()) != null) {
                            sb.append(line);
                        }
                        readedJson = sb.toString();
                        readed = true;
                    } catch (Exception e) {
                        readed = false;
                    }

                    if (readed) {
                        Gson gson = new GsonBuilder().create();
                        return gson.fromJson(readedJson, new TypeToken<List<WiFiPoint>>() {
                        }.getType());
                    }
                }

                ////////////////////////////////////////////////////////////////////////////////
                // Если файл не найден, то грузим с сервера ↓

                String url = "https://apidata.mos.ru/v1/datasets/" + Category + "/rows?api_key=" + getString(R.string.API_KEY_MOS);

                URL obj;
                StringBuffer response;
                obj = new URL(url);
                HttpURLConnection connection;
                connection = (HttpURLConnection) obj.openConnection();
                connection.setRequestMethod("GET");
                BufferedReader in;
                in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String inputLine;
                response = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
                Gson gson = new GsonBuilder().create();

                // Сохранение файла с данными /////////////////////////////////////
                FileOutputStream outputStream;
                outputStream = openFileOutput(filename, Context.MODE_PRIVATE);
                outputStream.write(response.toString().getBytes());
                outputStream.close();
                ///////////////////////////////////////////////////////////////////

                return gson.fromJson(response.toString(), new TypeToken<List<WiFiPoint>>() {}.getType());
            }
            catch (Exception e){
                return null;
            }

        }

        @Override
        protected void onPostExecute(List<WiFiPoint> result) {
            super.onPostExecute(result);

            if(result == null && !globals.refresh) {
                pBar.setVisibility(View.GONE);
                txtError.setText("Ошибка подключения!");
                txtError.setVisibility(View.VISIBLE);
                btnRetry.setVisibility(View.VISIBLE);
                return;
            }

            if(result == null && globals.refresh){
                txtError.setVisibility(View.GONE);
                btnRetry.setVisibility(View.GONE);
                loadWindow.setVisibility(View.GONE);
                listView.setVisibility(View.VISIBLE);
                fab.setVisibility(View.VISIBLE);
                Toast.makeText(getApplicationContext(), "Ошибка подключения!", Toast.LENGTH_SHORT).show();
                globals.refresh = false;
                return;
            }

            globals.refresh = false;

            pointList = new ArrayList<>(result.size());
            pointList.addAll(result);

            wifiListAdapter = new WifiListAdapter(pointList, getApplicationContext());
            listView.setAdapter(wifiListAdapter);

            // Обработка клика по элементу списка
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent mapIntent = new Intent(getApplicationContext(), MapActivity.class);

                    mapIntent.putExtra("lat", pointList.get(position).getCells().getGeoData().getCoordinates().get(0));
                    mapIntent.putExtra("lng", pointList.get(position).getCells().getGeoData().getCoordinates().get(1));
                    startActivity(mapIntent);
                }
            });

            txtError.setVisibility(View.GONE);
            btnRetry.setVisibility(View.GONE);
            loadWindow.setVisibility(View.GONE);
            listView.setVisibility(View.VISIBLE);
            fab.setVisibility(View.VISIBLE);
        }

    }



    // Категории данных - сопоставлены с их id в запросе
    public class DataCategory {
        final static int PARKS_WIFI = 861;
        final static int CITY_WIFI = 2756;
        final static int LIBRARY_WIFI = 60788;
        final static int CINEMA_WIFI = 60789;
        final static int CULTURAL_CENTER_WIFI = 60790;
    }
}