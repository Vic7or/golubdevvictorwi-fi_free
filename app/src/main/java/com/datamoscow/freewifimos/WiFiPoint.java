package com.datamoscow.freewifimos;


import java.util.List;

public class WiFiPoint {

    private int global_id;
    private int Number;

    private Cells Cells;

    public WiFiPoint(int global_id, int Number, Cells Cells){
        this.global_id = global_id;
        this.Number = Number;
        this.Cells = Cells;
    }

    public int getGlobal_id() {
        return global_id;
    }
    public void setGlobal_id(int global_id) {
        this.global_id = global_id;
    }
    public int getNumber() {
        return Number;
    }
    public void setNumber(int number) {
        Number = number;
    }
    public Cells getCells() {
        return Cells;
    }
    public void setCells(Cells cells) {
        Cells = cells;
    }

    class Cells{
        // Радиус действия ??
        private int CoverageArea;

        //Поля wifi в парках
        private String Name;

        // Административный округ
        private String AdmArea;

        // Район
        private String District;

        // Название парка
        private String ParkName;

        // Имя сети WiFi
        private String WiFiName;

        // Действует / Не действует
        private String FunctionFlag;

        // Открытая / закрытая сеть
        private String AccessFlag;

        // Закрытая сеть → пароль, открытая → null
        private String Password;

        // Адрес учреждения с WiFi
        private String Address;
        // или для городского Wifi
        private String Location;

        // дополнительные поля для городского wifi
        private int NumberOfAccessPoints;

        // Название библиотеки
        private String LibraryName;

        // Название Кинотеатра
        private String CinemaName;

        // Название культурного центра
        private String CulturalCenterName;

        // Гео. положение
        private geoData geoData;


        // Стандартные getter-ы и setter-ы
        public int getCoverageArea() {
            return CoverageArea;
        }
        public void setCoverageArea(int coverageArea) {
            CoverageArea = coverageArea;
        }
        public String getName() {
            return Name;
        }
        public void setName(String name) {
            Name = name;
        }
        public String getAdmArea() {
            return AdmArea;
        }
        public void setAdmArea(String admArea) {
            AdmArea = admArea;
        }
        public String getDistrict() {
            return District;
        }
        public void setDistrict(String district) {
            District = district;
        }
        public String getParkName() {
            return ParkName;
        }
        public void setParkName(String parkName) {
            ParkName = parkName;
        }
        public String getWiFiName() {
            return WiFiName;
        }
        public void setWiFiName(String wiFiName) {
            WiFiName = wiFiName;
        }
        public String getFunctionFlag() {
            return FunctionFlag;
        }
        public void setFunctionFlag(String functionFlag) {
            FunctionFlag = functionFlag;
        }
        public String getAccessFlag() {
            return AccessFlag;
        }
        public void setAccessFlag(String accessFlag) {
            AccessFlag = accessFlag;
        }
        public String getPassword() {
            return Password;
        }
        public void setPassword(String password) {
            Password = password;
        }
        public geoData getGeoData() {
            return geoData;
        }
        public void setGeoData(geoData geoData) {
            this.geoData = geoData;
        }
        public String getLocation() {
            return Location;
        }
        public void setLocation(String location) {
            Location = location;
        }
        public int getNumberOfAccessPoints() {
            return NumberOfAccessPoints;
        }
        public void setNumberOfAccessPoints(int numberOfAccessPoints) {
            NumberOfAccessPoints = numberOfAccessPoints;
        }
        public String getLibraryName() {
            return LibraryName;
        }
        public void setLibraryName(String libraryName) {
            LibraryName = libraryName;
        }
        public String getAddress() {
            return Address;
        }
        public void setAddress(String address) {
            Address = address;
        }
        public String getCinemaName() {
            return CinemaName;
        }
        public void setCinemaName(String cinemaName) {
            CinemaName = cinemaName;
        }
        public String getCulturalCenterName() {
            return CulturalCenterName;
        }
        public void setCulturalCenterName(String culturalCenterName) {
            CulturalCenterName = culturalCenterName;
        }


        // Дополнительные getter-ы и setter-ы
        public String getFullAddress(){
            if(getAddress() != null)
                return getAddress();
            if(getLocation() != null)
                return getLocation();
            return "Данный объект не имеет адреса. Вы можете посмотреть его местоположение на карте.";
        }
        public String getFullName(){
            if(getCinemaName() != null)
                return getCinemaName();
            if(getCulturalCenterName() != null)
                return getCulturalCenterName();
            if(getLibraryName() != null)
                return getLibraryName();
            if(getParkName() != null)
                return getParkName();
            if(getName() != null)
                return getName();
            return "Общедоступная точка Wi-Fi в Москве";
        }

        class geoData{
            // Тип - всегда Point ??
            private String type;

            // Координаты [0] и [1]
            private List<Double> coordinates;

            // Стандартные getter-ы и setter-ы

            public String getType() {
                return type;
            }
            public void setType(String type) {
                this.type = type;
            }
            public List<Double> getCoordinates() {
                return coordinates;
            }
            public void setCoordinates(List<Double> coordinates) {
                this.coordinates = coordinates;
            }
        }
    }

}


