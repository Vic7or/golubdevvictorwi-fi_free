package com.datamoscow.freewifimos;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class FilterActivity extends AppCompatActivity {

    Intent intent = new Intent();
    String[] pointList;
    String[] pointList2;
    Spinner spinner=null;
    Spinner spinnerAdmArea=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);

        pointList = getIntent().getStringArrayExtra("points1");
        pointList2 = getIntent().getStringArrayExtra("points2");
        int i = 0;
        ArrayList<String> areas = new ArrayList<String>();
        while (i < pointList.length){
            areas.add(pointList[i]);
            i++;
        }
        ArrayList<String> admareas = new ArrayList<String>();
        i=0;
        while (i < pointList2.length){
            admareas.add(pointList2[i]);
            i++;
        }
        spinner=(Spinner)findViewById(R.id.spinner);
        //Adapter
        ArrayAdapter<String> adapter=new ArrayAdapter<String>(getApplicationContext(),R.layout.custom_spinner,areas);
        //Определяем разметку для использования при выборе элемента
        adapter.setDropDownViewResource(R.layout.custom_drop_down);
        //android.R.layout.simple_spinner_dropdown_item
        //Применяем адаптер к spinner
        spinner.setAdapter(adapter);
        spinner.setSelection(0);


        spinnerAdmArea=(Spinner)findViewById(R.id.spinnerAdm);
        ArrayAdapter<String> adapter2=new ArrayAdapter<String>(getApplicationContext(),R.layout.custom_spinner,admareas);
        //Определяем разметку для использования при выборе элемента
        adapter2.setDropDownViewResource(R.layout.custom_drop_down);
        //Применяем адаптер к spinner
        spinnerAdmArea.setAdapter(adapter2);
        spinnerAdmArea.setSelection(0);
    }

    public void buttonWithFilterAreaClicked(View v){
        String AreaSpin=spinner.getSelectedItem().toString();
        //print(AreaSpin);
        intent.putExtra("Area", AreaSpin);
        intent.putExtra("AdmArea", "0");
        intent.putExtra("Without", "0");
        setResult(RESULT_OK, intent);
        this.finish();

    }
    public void buttonWithFilterAdmAreaClicked(View v){
        String AdmAreaSpin=spinnerAdmArea.getSelectedItem().toString();
        //print(AdmAreaSpin);
        intent.putExtra("Area", "0");
        intent.putExtra("AdmArea", AdmAreaSpin);
        intent.putExtra("Without", "0");
        setResult(RESULT_OK, intent);
        this.finish();
    }
    public void buttonWithOutFilterClicked(View v){
        //print("Без фильтра");
        intent.putExtra("Area", "0");
        intent.putExtra("AdmArea", "0");
        intent.putExtra("Without", "+");
        setResult(RESULT_OK, intent);
        this.finish();
    }
    public void print(String txt){
        Toast.makeText(getApplicationContext(),txt,Toast.LENGTH_SHORT).show();

    }

}
